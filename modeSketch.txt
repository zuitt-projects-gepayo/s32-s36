app: Booking System API
decript:
    Allows a user to enroll to a course. 
    Allows an Admin to do CRUD operations on courses
    Allows us to register regular users


User"
    firstName : String,
    lastName : String,
    email : String,
    password : String,
    mobileNo : String,
    isAdmin : boolean, (default : false)
    enrollments : 
    [
        {
            courseID : String,
            status: string,
            dateEnrolled: date
        }
    ]

Associate Entity:

Enrollment - Two way Embedding when there is a many to many relationship. It means the associative entity created must be embedded on both documents


course
    name : string,
    desription : string,
    price : number,
    isActive : boolean, default true
    createdOn : date,
    enrollees :
    [
        {
            userID: string,
            status: string,
            dateEnrolled:date
        }
    ]