const mongoose = require ('mongoose');

let userSchema = new mongoose.Schema({

    firstName : {
        type : String,
        requred: [true, "First Name is required"]
    },

    lastName :  {
        type: String,
        requred: [true, "Last Name is required"]
    },

    email : {
        type : String,
        requred: [true, "Email is required"]
    },

    password : {
        type : String,
        required : [true, "Password is required"]
    },

    mobileNo : {
        type : String,
        required : [true, "Mobile Number is required"]
    },

    isAdmin : {
        type : Boolean,
        default : false
    },

    enrollments : 
    [
        {
            courseId : {
                type : String,
                required : [true, "Course ID is Required"]
            },

            status : {
                type : String,
                default : "Enrolled"
            },

            dateEnrolled : {
                type: Date,
                default : new Date()
            }
        }
    ]
})

module.exports = mongoose.model("User",userSchema);