const express = require ('express');
const router = express.Router();


//import user controller
const userControllers = require("../controllers/userControllers");

const auth = require("../auth")

const {verify, verifyAdmin} = auth;
//Routes

//User Registration
router.post("/" , userControllers.registerUser);

//Retrieve all users
router.get("/", userControllers.getAllUsers);

//Login route
router.post("/login", userControllers.loginUser);

//Retrieve use Details
router.get("/getUserDetails",verify, userControllers.getUserDetails)

//check Email Exist
router.post("/checkEmailExists", userControllers.checkEmailExists)

//updating a regular user to admin
router.put("/updateAdmin/:id",verify ,verifyAdmin, userControllers.updateAdmin)

//update user details
router.put("/updateUserDetails", verify, userControllers.updateUserDetails);

//enroll registered user
router.post("/enroll", verify, userControllers.enroll);

//retrieve logged in user enrollments
router.get("/getEnrollments", verify, userControllers.getUserEnrollments)

module.exports = router;

