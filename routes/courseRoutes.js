const express = require ('express');
const router = express.Router();

//import course controller
const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth")

const {verify, verifyAdmin} = auth;

//Routes

//ADD courses
router.post("/" ,verify,verifyAdmin, courseControllers.addCourse);

//Retrieve all Courses
router.get("/", courseControllers.getAllCourses);

router.get("/getSingleCourse/:id",courseControllers.getSingleCourse);

//archive a Course
router.put("/archive/:id", verify, verifyAdmin, courseControllers.archiveCourse)

//active a course
router.put("/activate/:id", verify, verifyAdmin, courseControllers.activateCourse)

//Retrieve all active Courses
router.get("/getActiveCourses", courseControllers.getAllActiveCourses);

//update a course
router.put ("/:id", verify, verifyAdmin, courseControllers.updateCourse);

//Retrieve all inactive Courses
router.get("/getInActiveCourses",verify,verifyAdmin, courseControllers.getAllInActiveCourses);

//Find courses by name
router.post("/findCoursesByName", courseControllers.findCoursesByName);

//Find courses by Price
router.post ("/findCoursesByPrice", courseControllers.findCoursesByPrice);

//Find Course Enrollees List
router.get("/getEnrollees/:id", verify, verifyAdmin, courseControllers.getEnrollees)

module.exports = router;