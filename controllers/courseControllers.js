const Course = require("../models/Course");


//Add Courses
module.exports.addCourse = (req, res) => {

    console.log(req.body);


    let newCourse = new Course ({
        name : req.body.name,
        description : req.body. description,
        price : req.body.price,
    })

    newCourse.save()
    .then(course => res.send(course))
    .catch(err => res.send(err));

};

//Retrieve all Courses
module.exports.getAllCourses = (req, res) => {
    
    Course.find({})
    .then(course => res.send(course))
    .catch(err => res.send(err));
};


//Retrieve Single Course
module.exports.getSingleCourse = (req, res) => {

    console.log(req.params);
    
    Course.findById(req.params.id)
    .then(course => res.send(course))
    .catch(err => res.send(err));
};

//Archive a Course using an Admin User
module.exports.archiveCourse = (req,res) => {

    console.log(req.params.id);

    let updates = {
        isActive : false
    }

    Course.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then(updatedCourse => res.send(updatedCourse))
    .catch(err => res.send(err));
};

//Activate a Course using an Admin User
module.exports.activateCourse = (req,res) => {

    console.log(req.params.id);

    let updates = {
        isActive : true
    }

    Course.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then(updatedCourse => res.send(updatedCourse))
    .catch(err => res.send(err));
};

//Retrieve all Active Courses
module.exports.getAllActiveCourses = (req, res) => {

    Course.find({isActive : true})
    .then(activeCourses => res.send(activeCourses))
    .catch(err => res.send(err))
};

//Update A Course
module.exports.updateCourse = (req,res) => {

    console.log(req.params.id);
    console.log(req.body);

    let updateCourse = {
        name : req.body.name,
        description : req.body.description,
        price : req.body.price
    }

    Course.findByIdAndUpdate(req.params.id, updateCourse, {new: true})
    .then(updatedCourse => res.send(updatedCourse))
    .catch(err => res.send(err));
};

//Retrieve all inactive Courses
module.exports.getAllInActiveCourses = (req, res) => {

    Course.find({isActive : false})
    .then(activeCourses => res.send(activeCourses))
    .catch(err => res.send(err))
};

//Find Course By Name
module.exports.findCoursesByName = (req, res) => {

	Course.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {

		if(result.length === 0){
			return res.send("No courses found");
		} else {
			return res.send(result);
		}
	})
	.catch(err => res.send(err));
};



//find courses by price
module.exports.findCoursesByPrice = (req, res) => {

    Course.find({price: req.body.price})
    .then(result => {

        if (result.length === 0){
            return res.send("No course Found");
        }
        else{
            return res.send(result);
        }
    })

    .catch (err => res.send (err));
};

//Get Enrollee's List
module.exports.getEnrollees = (req, res) => {

    console.log(req.params.id)

    Course.findById(req.params.id)
    .then(course => res.send(course.enrollees))
    .catch(err => res.send(err))
};