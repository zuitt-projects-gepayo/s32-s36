//import bcrypt module
const bcrypt = require ('bcrypt');

//Import the User Model
const User = require("../models/User");

//Import auth module
const auth = require ("../auth");

const Course = require('../models/Course');


//Controllers

//User Registration
module.exports.registerUser = (req, res) => {

    console.log(req.body);

    /*
        bcrypt = add a layer of security to our user's password

        Whay brcrypt does is has our password into a randomized character version of  the original string

        SYNTAX
            bcrypt.hashSync(<stringToBeHashed>, saltRounds)

            salt - Rounds ar the number of times the characters in the hash are randomized
    */

    const hashedPW = bcrypt.hashSync(req.body.password, 10)

     //Create a new User document out of our user model

    let newUser = new User ({
        firstName : req.body.firstName,
        lastName : req.body. lastName,
        email : req.body.email,
        mobileNo : req.body.mobileNo,
        password : hashedPW
    })

    newUser.save()
    .then(user => res.send(user))
    .catch(err => res.send(err));

};

//Retrieve all Users
module.exports.getAllUsers = (req,res) =>{

    User.find({})
    .then(user => res.send(user))
    .catch(err => res.send(err));
}

//Login User
module.exports.loginUser = (req, res) => {

    console.log(req.body);

    /*
        1. find the user email
        2. if user email is found, check the password
        3. if we dont find the  user email, send a messge 
        4. if upon checking the found user's password is athe same as our input password, we will generate a "key" to access our app. if not, we will turn him away by sending a message to the client
    */

        User.findOne({email: req.body.email})
        .then(foundUser => {
            if(foundUser === null){
                return res.send("user does not exist");
            }
            else{
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

                if(isPasswordCorrect){
                    return res.send({accessToken : auth.createAccessToken(foundUser)})
                }
                else{
                    return res.send("Password is incorrect")
                }
            }
        })
        .catch(err => res.send(err));
};

//Get User Details
module.exports.getUserDetails = (req, res) => {

    console.log(req.user)

    //1. find a login in user's document from our db and sent it tothe client by its id

    User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(err => res.send(err));
};


//Check Email Exists
module.exports.checkEmailExists  = (req,res) => 
{
    console.log(req.body);

    User.findOne({email: req.body.email})
    .then(resultemail => {
        if(resultemail !== null && resultemail.email === req.body.email)
        {
            return res.send('Email Already Registered');
            
        }
        else
        {
           return res.send("Email is Available");
        }
    })
    
    // .then(resultemail => res.send(resultemail))
    
    .catch(error => res.send(error))
};

//Updating a regular use to Admin
module.exports.updateAdmin = (req, res) => {

    console.log(req.params.id);

    let updates = {
        isAdmin : true
    };

    User.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then(updatedUser => res.send(updatedUser))
    .catch(err => res.send(err));
};

//update user details
module.exports.updateUserDetails = (req,res) => {

    console.log(req.body); // checking the input for new values for our user's details

    console.log(req.user.id)//check the logged in user's ID

    let updates = {
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        mobileNo : req.body.mobileNo
    };

    User.findByIdAndUpdate(req.user.id, updates, {new : true})
    .then(updatedUser => res.send(updatedUser))
    .catch(err => res.send(err))
};


//ENROLL TO A course
module.exports.enroll = async (req,res) =>{

    console.log(req.user.id);
    console.log(req.body.courseId);

    if(req.user.isAdmin){
        return res.send("Action Forbidden");
    }
    /*
		Enrollment Process:

		1. Look for the user by its id.
			>> push the details of the course we're trying to enroll in.
				>> we'll push to a new enrollment subdocument in our user

		2. Look for the course by its id.
			>> push the details of the enrollee/ user who's trying to enroll.
				>> we'll push to a new enrollees subdocument in our course

		3. When both saving documents are successful, we send a message to the client 
	*/

    let isUserUpdated = await User.findById(req.user.id)
    .then(user => {

        console.log(user)

        let newEnrollment = {
            courseId: req.body.courseId
        }

        user.enrollments.push(newEnrollment);

        return user.save()
        .then(user => true)
        .catch(err => err.message);
    })

    //if isUserUpdated does not contain the boolean value of true, we will stop our process and return re.send() to our client with our message.
    if(isUserUpdated !== true){

        return res.send({message: isUserUpdated})
    }

    let isCourseUpdated = await Course.findById(req.body.courseId)
    .then(course => {
        
        console.log(course);

        let enrollee = {
            userId : req.user.id
        }
        course.enrollees.push(enrollee);
        return course.save()
        .then (course => true)
        .catch(err => err.message);
    })

    if(isCourseUpdated !== true){

        return res.send({message: isCourseUpdated})
    }

    if(isUserUpdated && isCourseUpdated ){
        
        return res.send({message: "Enrolled Successfully!"})
    }
};

//Retrieve Logged in User Enrollments
module.exports.getUserEnrollments = (req,res) =>{

    User.findById(req.user.id)

    .then(user => res.send(user.enrollments))
    .catch(err => res.send(err))

};